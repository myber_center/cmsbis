<script src="<?php echo base_url('include/template/limitless/')?>/global_assets/js/plugins/forms/selects/select2.min.js"></script>
<form class="form-horizontal" action="<?php echo $data_get['param']['table'] ?>/update_data" id="app-submit" method="POST">

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-body">
			<fieldset>
				<input type="hidden" value="<?php echo $data_get['data_edit']['id_faq'] ?>" name="id_faq">

	            <div class="form-group">
	              <label class="col-lg-3 control-label">Question:</label>
	              <div class="col-lg-9">
	                <input type="text" name="question" value="<?php echo $data_get['data_edit']['question'] ?>" class="form-control" required placeholder="Input here......">
	              </div>
	            </div>
	            <div class="form-group">
	              <label class="col-lg-3 control-label">Answer:</label>
	              <div class="col-lg-9">
	              	<textarea class="form-control" name="answer"><?php echo $data_get['data_edit']['answer'] ?></textarea>
	              </div>
	            </div>
	            <button class="btn btn-success" type="submit">Simpan</button>
            </fieldset>	
		</div>
	</div>
</div>
</form>