	<!-- About Us -->
		<section class="about-us section-space">
			<div class="container">
				<div class="row">
					<div class="col-lg-5 offset-lg-1 col-md-6 col-12">
						<!-- About Video -->
							
								<iframe width="790" height="435" src="https://www.youtube.com/embed/GQOzOEt4BJU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
								
						<!--/End About Video  -->
					</div>
					<div class="col-lg-5 col-md-6 col-12">
						<div class="about-content section-title default text-left">
							<div class="section-top">
								<h1><b>What is CCW?</b></h1>
							</div>
							<div class="section-bottom">
								<div class="text">
									<p>We are serious about the quality of our coconut shell charcoal briquettes and utilize the strictest quality control measures at all stages of production, from shell selection to final packaging of the briquettes. We believe the charcoal used in briquettes is the most important factor determining quality of the final product. We produce our own raw charcoal from carefully selected coconut shells. Our production process results in charcoal with a high fixed carbon content and low volatile matter and ash content.</p>
								</div>
								<div class="button">
									<a href="about.html" class="bizwheel-btn theme-2">Our Works<i class="fa fa-angle-right"></i></a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>	
		<!--/ End About Us -->