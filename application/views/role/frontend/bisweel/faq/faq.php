
		<!-- Start Error Area-->
		<section class="error">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 ol-md-12 col-12">
						<div class="error-inner">
							<h4>FAQ</h4>
							<h2>Frequently Ask <span>Question</span></h2>
							
							<?php foreach ($faq as $key => $value): ?>
								<div class="faq">
									<b><?php echo $value['question'] ?></b>
									<p>"<?php echo $value['answer'] ?>"</p>
								</div>
							<?php endforeach ?>
							<div class="button">
								<a href="#" class="bizwheel-btn"><i class="fa fa-long-arrow-left"></i>Go to home</a>
							</div>
						</div>
					</div>
					
				</div>
			</div>
		</section>
		<!--/ End Error Area-->
	