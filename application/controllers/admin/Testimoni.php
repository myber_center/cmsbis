<?php
defined('BASEPATH') OR exit('No direct script access allowed');
		use PhpOffice\PhpSpreadsheet\Spreadsheet;
		use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class testimoni extends MY_Controller {

	public $arr = [
			'title'				=>	'Halaman testimoni',
			'table'				=>	'testimoni',
			'column'			=>	[ 'img','keterangan','nama','testimoni'],
			'column_order'		=>	[ 'id_testimoni','img','keterangan','nama','testimoni'],
			'column_search'		=>	[ 'id_testimoni','img','keterangan','nama','testimoni'],
			'order'				=>	['id_testimoni'	=>	'DESC'],
			'id'				=>	'id_testimoni'
	];

	/*
		CHANGE PAGE
	*/
	public function get_data()
	{
		/*if you need custom page*/

		$data['account']	=	$this->get_user_account();
		$data['param'] 		= 	$this->arr;
		$this->my_view(['role/admin/page/testimoni/index_page/index','role/admin/page/testimoni/index_page/js'],$data);

	}

	public function add_page()
	{
		$data['account']	=	$this->get_user_account();
		$data['param'] 		= 	$this->arr;
		$this->my_view(['role/admin/page/testimoni/add_page/index','role/admin/page/testimoni/add_page/js'],$data);
	}

	public function edit_page($id)
	{
		$dt = $this->arr;

		$data['param'] 		= 	$this->arr;
		if (isset($id)) {
			$data_set = $this->my_where($dt['table'],[$dt['id']=>$id])->row_array();
			$data['data_edit']	=	$data_set;
			$this->my_view(['role/admin/page/testimoni/edit_page/index','role/admin/page/testimoni/edit_page/js'],$data);
		} else {
			$this->get_data();
		}
	}

	/*
		ADD DATA 
	*/


	public function simpan_data()
	{	
		if(file_exists($_FILES['img']['tmp_name']) || is_uploaded_file($_FILES['img']['tmp_name'])) {
			$img = $this->save_media([
				'path'	=>	"./include/media/",
				'filename' => 'img',
			]);
		}

		$data = [
			'nama'					=>	$_POST['nama'],
			'keterangan'			=>	$_POST['keterangan'],	
			'testimoni'				=>	$_POST['testimoni'],	
			'img'					=>	((isset($img)) ? $img['file_name'] : ''),
		];

		$this->save_data('testimoni', $data);
	}


	/*
		EDIT DATA
	*/

	function update_data()
	{
		if (isset($_POST)) {
			if(file_exists($_FILES['img']['tmp_name']) || is_uploaded_file($_FILES['img']['tmp_name'])) {
					$img = $this->save_media([
						'path'	=>	"./include/media/",
						'filename' => 'img',
					]);
				}
		$data = [
			'nama'					=>	$_POST['nama'],
			'keterangan'			=>	$_POST['keterangan'],	
			'testimoni'				=>	$_POST['testimoni'],	
		];

			if(file_exists($_FILES['img']['tmp_name']) || is_uploaded_file($_FILES['img']['tmp_name'])) {
				$data['img'] = ((isset($img)) ? $img['file_name'] : '');
			}
			$this->my_update(
				'testimoni', 
				$data,
				['id_testimoni'=>$_POST['id_testimoni']]
			);
		}
	}

	/*
		DELETE DATA
	*/

	function hapus()
	{
		$dt = $this->arr;
		foreach ($_POST['data_get'] as $key => $value) {
			$this->db->delete($dt['table'],[$dt['id']=>$value]);
		}
	}


	public function datatable()
	{
		$_POST['frm']   =   $this->arr;
        $list           =   $this->mod_datatable->get_datatables();
        $data           =   array();
        $no             =   $_POST['start'];
        foreach ($list as $field) {
            $no++;
            $row        =   array();
            
            $row[]      =   '<input type="checkbox" name="get-check" value="'.$field['id_testimoni'].'"></input>';
            $row[]		=	'<img src="'.(base_url('include/media/'.$field['img'])).'" style="width:50%"> ';
            $row[]		=	$field['nama'];
            $row[]		=	$field['keterangan'];
            $row[]		=	$field['testimoni'];
            $data[]     =   $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->mod_datatable->count_all(),
            "recordsFiltered" => $this->mod_datatable->count_filtered(),
            "data" => $data,
        );

        echo json_encode($output);
	}
	
	
}