
		
		<!-- Latest Blog -->
		<section class="latest-blog section-space">
			<div class="container">
				<div class="row">
					<div class="col-lg-6 offset-lg-3 col-md-8 offset-md-2 col-12">
						<div class="section-title default text-center">
							<div class="section-top">
								<h1><?php echo $setting_table['blog_post_title'] ?></h1>
							</div>
							<div class="section-bottom">
								<div class="text">
									<p><?php echo $setting_table['blog_post_head_text'] ?></p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-12">
						<div class="blog-latest blog-latest-slider">
							<?php foreach ($blog_post as $key => $value): ?>
								<div class="single-slider">
									<!-- Single Blog -->
									<div class="single-news ">
										<div class="news-head overlay">
											<span class="news-img" style="background-image:url(<?php echo base_url('include/media/'.$value['img']) ?>)"></span>
											<a href="<?php echo base_url('frontend/detail_blog/'.$value['id_blog_post']) ?>" class="bizwheel-btn theme-2">Read more</a>
										</div>
										<div class="news-body">
											<div class="news-content">
												<h3 class="news-title"><a href="blog-single.html"><?php echo $value['title'] ?></a></h3>
												<div class="news-text"><?php echo substr($value['blog_post'], 0,100) ?>...</div>
												<ul class="news-meta">
													<li class="date"><i class="fa fa-calendar"></i><?php echo date_format(date_create($value['create_at']), 'd M Y') ?></li>
													<li class="view"><i class="fa fa-comments"></i>0</li>
												</ul>
											</div>
										</div>
									</div>
									<!--/ End Single Blog -->
								</div>
							<?php endforeach ?>
							
						</div>
					</div>
				</div>
			</div>
		</section>
		<!--/ End Latest Blog -->