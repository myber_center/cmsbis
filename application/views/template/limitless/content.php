

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><span class="text-semibold">Master</span> - Education</h4>
						</div>

						<!-- <div class="heading-elements">
							<div class="heading-btn-group">
								<a href="#" class="btn btn-link btn-float text-size-small has-text"><i class="icon-add text-primary"></i><span>Add Data</span></a>
								<a href="#" class="btn btn-link btn-float text-size-small has-text"><i class="icon-trash text-primary"></i> <span>Delete</span></a>
								<a href="#" class="btn btn-link btn-float text-size-small has-text"><i class="icon-pencil text-primary"></i> <span>Edit</span></a>
							</div>
						</div> -->
					</div>

					<div class="breadcrumb-line breadcrumb-line-component">
						<ul class="breadcrumb">
							<strong><i><?php echo $profil_website['breadcrumb_elements'] ?></i></strong>
						</ul>

						<ul class="breadcrumb-elements">
							<li><a href="#"><i class="icon-comment-discussion position-left"></i> Support</a></li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<i class="icon-gear position-left"></i>
									Settings
									<span class="caret"></span>
								</a>

								<ul class="dropdown-menu dropdown-menu-right">
									<li><a href="#"><i class="icon-user-lock"></i> Setting</a></li>
									<li><a href="#"><i class="icon-statistics"></i> Profil Website</a></li>
									<li><a href="#"><i class="icon-accessibility"></i> Profil User</a></li>
									<li class="divider"></li>
									<li><a href="<?php echo base_url('auth/logout') ?>"><i class="icon-gear"></i> Logout</a></li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
				<!-- /page header -->


				<!-- Content area -->
				<div class="content">
					<div class="app-content">
					</div>

