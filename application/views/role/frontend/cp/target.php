
    <!-- breadcrumb start-->
    <section class="breadcrumb breadcrumb_bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb_iner text-center">
                        <div class="breadcrumb_iner_item">
                            <h2>Target Milenial Indonesia Bangkit</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- breadcrumb start-->

    <!-- about_us part start-->
    <section class="about_us section_padding">
        <div class="container">
            <div class="row align-items-center justify-content-between">
                <div class="col-md-12 col-lg-12">
                    <div class="learning_img">
                        <img src="<?php echo base_url('include/template/sasu/img/')?>h1.png" alt="">
                    </div>
                </div>
            </div>
            <div class="row align-items-center justify-content-between">
                <div class="col-md-12 col-lg-12">
                    <div class="learning_img">
                        <img src="<?php echo base_url('include/template/sasu/img/')?>h2.png" alt="">
                    </div>
                </div>
            </div>
            <div class="row align-items-center justify-content-between">
                <div class="col-md-12 col-lg-12">
                    <div class="learning_img">
                        <img src="<?php echo base_url('include/template/sasu/img/')?>h3.png" alt="">
                    </div>
                </div>
            </div>
            <div class="row align-items-center justify-content-between">
                <div class="col-md-12 col-lg-12">
                    <div class="learning_img">
                        <img src="<?php echo base_url('include/template/sasu/img/')?>h4.png" alt="">
                    </div>
                </div>
            </div>
            <div class="row align-items-center justify-content-between">
                <div class="col-md-12 col-lg-12">
                    <div class="learning_img">
                        <img src="<?php echo base_url('include/template/sasu/img/')?>h5.png" alt="">
                    </div>
                </div>
            </div>
            <div class="row align-items-center justify-content-between">
                <div class="col-md-12 col-lg-12">
                    <div class="learning_img">
                        <img src="<?php echo base_url('include/template/sasu/img/')?>h6.png" alt="">
                    </div>
                </div>
            </div>
            <div class="row align-items-center justify-content-between">
                <div class="col-md-12 col-lg-12">
                    <div class="learning_img">
                        <img src="<?php echo base_url('include/template/sasu/img/')?>h7.png" alt="">
                    </div>
                </div>
            </div>
            <div class="row align-items-center justify-content-between">
                <div class="col-md-12 col-lg-12">
                    <div class="learning_img">
                        <img src="<?php echo base_url('include/template/sasu/img/')?>h8.png" alt="">
                    </div>
                </div>
            </div>
            
            
        </div>
       
    </section>
    <!-- about_us part end-->
