
		<!-- Call To Action -->
		<section class="call-action" style="background: <?php echo $setting_table['ad_section_color'] ?> !important;">
			<div class="container">
				<div class="row">
					<div class="col-lg-9 col-12">
						<div class="call-inner">
							<h2><?php echo $setting_table['ad_section_text_title'] ?></h2>
							<p><?php echo $setting_table['ad_section_text_body'] ?></p>
						</div>
					</div>
					<div class="col-lg-3 col-12">
						<div class="button">
							<a href="portfolio.html" class="bizwheel-btn">Our Portfolio</a>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!--/ End Call to action -->
	