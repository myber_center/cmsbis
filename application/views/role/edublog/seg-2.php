

<!--  Creative Start  -->
<section id="creative" class="padding_bottom padding_top">
	<div class="container">

		<div class="row">
			<div class="col-md-12 text-center heading_space heading">
				<h2><?= $sec2_title ?></h2>
				<p><?= $sec2_text ?></p>
			</div>
		</div>
		<div class="row">
			<?php foreach ($keunggulan as $value): ?>
				<div class="col-md-4 col-sm-4 col-xs-12 p-t-35">
					<div class="creative_detail">
						<div class="tag">
							<i class="fa fa-pencil" aria-hidden="true"></i>
						</div>
						<h3><?= $value['keunggulan'] ?></h3>
						<p><?= $value['keterangan'] ?></p>
					</div>
				</div>
			<?php endforeach ?>
			
		</div>
	</div>
</section>
<!--  Creative End  -->


