
<!--  Client Feedback Start  -->
<section id="client_feedback" class="padding_bottom">
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-center">
				<div class="heading margin_bottom">
					<h2>Clients Feedback</h2>
					<p>Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus. Nam libero tempore</p>
				</div>
			</div>
		</div>
		<div class="row">
			<div id="testinomial_slider" class="owl-carousel owl-theme">
				<div class="item">
					<div class="col-md-6 col-sm-6 col-xs-12 text-center">
						<div class="client_img">
							<img src="<?php echo base_url('include/template/edublog/images/')?>Client-1.png" class="client" alt="image">
							<h4>Adipisicing Elit</h4>
							<a href="javascript:void(0)"><i class="fa fa-user-o" aria-hidden="true"></i> Sit Amet / Director hoster.com</a>
							<br>
							<img src="<?php echo base_url('include/template/edublog/images/')?>stars.png" alt="image">
						</div>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<div class="client_details">
							<p>Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus. Nam libero tempore.Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit </p>
						</div>
					</div>
				</div>
				<div class="item">
					<div class="col-md-6 col-sm-6 col-xs-12 text-center">
						<div class="client_img">
							<img src="<?php echo base_url('include/template/edublog/images/')?>Client-2.png" class="client" alt="image">
							<h4>Adipisicing Elit</h4>
							<a href="javascript:void(0)"><i class="fa fa-user-o" aria-hidden="true"></i> Sit Amet / Director hoster.com</a>
							<br>
							<img src="<?php echo base_url('include/template/edublog/images/')?>stars.png" alt="image">
						</div>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<div class="client_details">
							<p>Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus. Nam libero tempore.Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit </p>
						</div>
					</div>
				</div>
				<div class="item">
					<div class="col-md-6 col-sm-6 col-xs-12 text-center">
						<div class="client_img">
							<img src="<?php echo base_url('include/template/edublog/images/')?>Client-3.png" class="client" alt="image">
							<h4>Adipisicing Elit</h4>
							<a href="javascript:void(0)"><i class="fa fa-user-o" aria-hidden="true"></i> Sit Amet / Director hoster.com</a>
							<br>
							<img src="<?php echo base_url('include/template/edublog/images/')?>stars.png" alt="image">
						</div>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<div class="client_details">
							<p>Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus. Nam libero tempore.Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit </p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!--  Client Feedback End  -->
