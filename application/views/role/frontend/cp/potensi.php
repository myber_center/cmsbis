
    <!-- breadcrumb start-->
    <section class="breadcrumb breadcrumb_bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb_iner text-center">
                        <div class="breadcrumb_iner_item">
                            <h2>Potensi Indonesia</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- breadcrumb start-->

    <!-- about_us part start-->
    <section class="about_us section_padding">
        <div class="container">
            <div class="row align-items-center justify-content-between">
                <div class="col-md-12 col-lg-12">
                    <div class="learning_img">
                        <img src="<?php echo base_url('include/template/sasu/img/')?>p1.png" alt="">
                    </div>
                </div>
            </div>
            <div class="row align-items-center justify-content-between">
                <div class="col-md-12 col-lg-12">
                    <div class="learning_img">
                        <img src="<?php echo base_url('include/template/sasu/img/')?>p2.png" alt="">
                    </div>
                </div>
            </div>
            <div class="row align-items-center justify-content-between">
                <div class="col-md-12 col-lg-12">
                    <div class="learning_img">
                        <img src="<?php echo base_url('include/template/sasu/img/')?>p3.png" alt="">
                    </div>
                </div>
            </div>
            <div class="row align-items-center justify-content-between">
                <div class="col-md-12 col-lg-12">
                    <div class="learning_img">
                        <img src="<?php echo base_url('include/template/sasu/img/')?>p4.png" alt="">
                    </div>
                </div>
            </div>
            <div class="row align-items-center justify-content-between">
                <div class="col-md-12 col-lg-12">
                    <div class="learning_img">
                        <img src="<?php echo base_url('include/template/sasu/img/')?>p5.png" alt="">
                    </div>
                </div>
            </div>
            <div class="row align-items-center justify-content-between">
                <div class="col-md-12 col-lg-12">
                    <div class="learning_img">
                        <img src="<?php echo base_url('include/template/sasu/img/')?>p6.png" alt="">
                    </div>
                </div>
            </div>
            <div class="row align-items-center justify-content-between">
                <div class="col-md-12 col-lg-12">
                    <div class="learning_img">
                        <img src="<?php echo base_url('include/template/sasu/img/')?>p7.png" alt="">
                    </div>
                </div>
            </div>
            
        </div>
       
    </section>