
	<nav class="navbar navbar-default no-background navbar-sticky bootsnav">
		<div class="container">
			<!-- Start Header Navigation -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
						<i class="fa fa-bars"></i>
					</button>
				<a class="navbar-brand mobile-logo" href="javascript:void(0)"><img src="<?php echo base_url('include/media/'.$template_setting[array_search('logo_head', $template_setting)]['value'])?>" alt="logo" class="logo" /></a>
			</div>
			<!-- End Header Navigation -->
			<div class="collapse navbar-collapse nav_1 clearfix" id="navbar-menu">
				<ul class="nav navbar-nav navbar-center" data-in="fadeInDown" data-out="fadeOutUp">
					<li class="dropdown active">
						<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">Home</a>
						<ul class="dropdown-menu">
							<!-- <li><a href="creative-home.html">Creative Home Page</a>
							</li>
							<li class="active"><a href="corporative-home.html">Corporative Home Page</a>
							</li>
							<li><a href="logistic-home.html">Logistic Home Page</a>
							</li>
							<li><a href="e-commerce-home.html">E - Commerce Homa Page</a>
							</li>
							<li><a href="charity-home.html">Charity Home Page</a>
							</li>
							<li><a href="gym-home.html">Real Gym Home Page</a>
							</li>
							<li><a href="portfolio-home.html">Portfolio Home Page</a>
							</li>
							<li><a href="blog-home.html">Blog Home Page</a>
							</li> -->
						</ul>
					</li>

					<li class="dropdown">
						<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">About Us</a>
						<ul class="dropdown-menu">
							<!-- <li><a href="about-1.html">About Us Style - 1</a>
							</li>
							<li><a href="about-2.html">About Us Style - 2</a>
							</li> -->
						</ul>
					</li>

					<!-- <li><a href="contact-us.html">Contact Us</a> -->
					</li>
				</ul>
			</div>
		</div>
	</nav>
</header>
<!--  Header End  -->

