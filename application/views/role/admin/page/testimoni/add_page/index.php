<script type="text/javascript" src="<?php echo base_url('include/ckeditor/ckeditor.js'); ?>"></script>
<form class="form-horizontal" action="<?php echo $data_get['param']['table'] ?>/simpan_data" id="app-submit" method="POST">

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-body">
			<fieldset>
	            <div class="form-group">
	              <label class="col-lg-3 control-label">Nama:</label>
	              <div class="col-lg-6">
	                <input type="text" name="nama" class="form-control" required placeholder="Input here......">
	              </div>
	            </div>
	            <div class="form-group">
	              <label class="col-lg-3 control-label">Keterangan/Jabatan:</label>
	              <div class="col-lg-9">
	              	<input type="text" name="keterangan" class="form-control" required placeholder="Input here......">
	              </div>
	            </div>
	            <div class="form-group">
	              <label class="col-lg-3 control-label">Testimoni:</label>
	              <div class="col-lg-9">
	              	<input type="text" name="testimoni" class="form-control" required placeholder="Input here......">
	              </div>
	            </div>
	           
	            <div class="form-group">
	              <label class="col-lg-3 control-label">Foto (Cover):</label>
	              <div class="col-lg-6">
	                <input type="file" name="img"  required class="form-control" required placeholder="Input here......">
	              </div>
	            </div>
	            
	            <button class="btn btn-success" type="submit">Simpan</button>
            </fieldset>	
		</div>
	</div>
</div>
</form>