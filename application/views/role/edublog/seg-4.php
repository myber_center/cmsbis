
<!--  Team Start  -->
<section id="team" class="padding_top padding_bottom">
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-center heading heading_space">
				<h2><?= $sec4_title ?></h2>
				<p><?= $sec4_text ?></p>
			</div>
		</div>

		<div class="row">
			<div id="team_slider">
				<?php foreach ($tim as $value): ?>
					
				
				<div class="item">
					<div class="team-effect">
						<figure class="wpf-demo-team">
							<a href="javascript:void(0)"><img src="<?php echo base_url('include/media/'.$value['foto'])?>" alt="img"></a>
							<figcaption class="view-caption text-center">
								<h4><?= $value['nama'] ?></h4>
								<p><?= $value['keterangan'] ?></p>
								<ul class="top_socials">
									<li><a href="javascript:void(0)"><i class="fa fa-facebook"></i></a></li>
									<li><a href="javascript:void(0)"><i class="fa fa-twitter"></i></a></li>
									<li><a href="javascript:void(0)"><i class="fa fa-youtube"></i></a></li>
									<li><a href="javascript:void(0)"><i class="fa fa-instagram"></i></a></li>
									<li><a href="javascript:void(0)"><i class="fa fa-pinterest"></i></a></li>
								</ul>
							</figcaption>
						</figure>
						<div class="team_detail">
							<h3><?= $value['panggilan'] ?></h3>
							<span><?= $value['jabatan'] ?></span>
						</div>
					</div>
				</div>
				<?php endforeach ?>
			</div>
		</div>
	</div>
</section>
<!--  Team End  -->

